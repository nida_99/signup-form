import React from 'react';
import Form from './Components/Form';
import './App.css';
import Nav from './Components/Nav';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import About from './Components/About';
import LandingPage from './Components/LandingPage';


class App extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (

      <>

        <Routes>
          <Route path="/about" element={<About />} exact />
          <Route path="/" element={<Form />} exact />
          <Route path="/pages" element={<LandingPage />} exact />
        </Routes>

      </>
    );
  };
};

export default App;
