import React from 'react';

class Header extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <header className='header'>
                <img src="https://icon-library.com/images/sign_up_png_1242083.png" alt="signupLogo"></img>
                SIGNUP FORM
            </header>
        );
    };
};

export default Header;
