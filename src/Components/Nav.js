import React from 'react';
import { Link } from 'react-router-dom'

class Nav extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <>
                <ul className="nav justify-content-center border border-light border-bottom-5 p-2">
                    <li className="nav-item">
                        <Link to='/about' className='px-4 m-1 text-dark fw-bold'>ABOUT US</Link>

                    </li>
                    <li className="nav-item">
                        <Link to='/' className='px-4 m-1 text-dark fw-bold'>SIGNUP</Link>
                    </li>
                    <li className="nav-item">
                        <Link to='/pages' className='px-4 m-1 text-dark fw-bold'>PAGES</Link>
                    </li>
                </ul>
            </>
        );
    };
};

export default Nav;