import React from 'react';
import Footer from './Footer';
import './pages.css'
import Nav from './Nav'

class About extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <>
                <Nav />
                <div className="container-i">
                    <h1 className='header'>ABOUT US</h1>
                </div>

                <p className="para"> Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Et aspernatur impedit enim a maiores! Ut voluptates
                    pariatur dolorum voluptate nesciunt accusantium saepe
                    fugit magni, aliquid sunt similique delectus eveniet
                    vitae. Officia minus aspernatur iste pariatur tempora,
                    perferendis veritatis amet esse obcaecati reiciendis
                    nam inventore sequi fugit placeat, laudantium
                    voluptate doloremque!</p>

                <Footer />

            </>
        );
    };
};

export default About;

